﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	Animator anim;
	public bool dead = false;
	public GameObject deathAnim;
	public Transform deathSpot;
	public GameObject purplePower;
	public GameObject greenPower;
	public GameObject bluePower;


	
	[System.Serializable]
	
	public class PlayerStats {
		public float Health = 100f;
	}

	public PlayerStats playerStats = new PlayerStats();

//	public int fallBoundary = -20;

	void Start () {
		anim = GetComponent<Animator> ();
		dead = false;
	}
	
	void Update(){
//		if (transform.position.y <= fallBoundary)
//			DamagePlayer (999999);

	}
	
	void OnCollisionEnter2D (Collision2D  collision){

		if (collision.gameObject.name == "fallout"){
			DamagePlayer (9999999);
			dead = true;

		}
		
		if (collision.transform.tag == "movingPlatform") 
		{
			transform.parent = collision.transform;
		}

	}

	void OnCollisionExit2D (Collision2D  collision){
		
		if (collision.transform.tag == "movingPlatform") 
		{
			transform.parent = null;
		}
	}

	void OnTriggerEnter2D (Collider2D trapCollider)
	{
		if (trapCollider.tag == "trap") {
			//yield return new WaitForSeconds(trapDelay);
			DamagePlayer (999999);
		}
			
		if (trapCollider.tag == "extinguish"){
			//yield return new WaitForSeconds(trapDelay);
			Debug.Log ("derp");
			DamagePlayer (99999);
			
		}

		if (trapCollider.gameObject.name == "fallout"){
			Debug.Log("derp");
			DamagePlayer (9999999);
			//anim.SetBool ("dead", dead);
			dead = true;
			
		}

		if (trapCollider.gameObject.tag == "enemy"){
			anim.SetTrigger ("takingDmg");
			Debug.Log("Took dmg");
			DamagePlayer(30);
		}

		if (trapCollider.gameObject.tag == "purplePower") 
		{
			Instantiate (purplePower, deathSpot.position, deathSpot.rotation);
			Destroy (gameObject);
		}

		if (trapCollider.gameObject.tag == "greenPower") 
		{
			Instantiate (greenPower, deathSpot.position, deathSpot.rotation);
			Destroy (gameObject);
		}

		if (trapCollider.gameObject.tag == "bluePower") 
		{
			Instantiate (bluePower, deathSpot.position, deathSpot.rotation);
			Destroy (gameObject);
		}

	}

	void playAnim()
	{

		dead = true;
		Debug.Log ("function on");

	}
	


	public void DamagePlayer (int damage) {
		playerStats.Health -= damage;

		if (playerStats.Health <= 0) {

			Destroy (this.gameObject);
			Instantiate (deathAnim, deathSpot.position, deathSpot.rotation);

			Debug.Log ("critdmg");
			GameMastr2.KillPlayer(this);


		}

	}


}
