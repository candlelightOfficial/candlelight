﻿using UnityEngine;
using System.Collections;

public class bluePowerup : MonoBehaviour {
	
	public Transform playerPrefab;
	public Transform spawnPoint;
	public int i = 0;
	public float powerUpTime;
	public float powerUpD;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		StartCoroutine (QueueSpawn ());
		Destroy (gameObject, powerUpD);
		
	}
	
	IEnumerator QueueSpawn() 
	{
		yield return new WaitForSeconds(powerUpTime);
		Debug.Log("reverting");
		
		if (i == 0) {
			Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
			i++;
		}
	}
}
