﻿using UnityEngine;
using System.Collections;

public class flamePickup : MonoBehaviour {

	public int pointsToAdd;

	void OnTriggerEnter2D(Collider2D other)
	{
		//if (other.GetComponent<Derp> () == null) 
		//return;

		if (other.tag == "Player") {

			FlameManager.AddFlame (pointsToAdd);

			Destroy (gameObject);


		}
	}
}
