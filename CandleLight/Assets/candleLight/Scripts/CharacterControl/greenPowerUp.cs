﻿using UnityEngine;
using System.Collections;

public class greenPowerUp : MonoBehaviour {


	public Transform playerPrefab;
	public Transform spawnPoint;

	public Transform dashSpawn;
	public Transform dashSprite;
	
	public Transform dashSpawn2;
	public Transform dashSprite2;
	
	public Transform dashSpawn3;
	public Transform dashSprite3;
	
	public Transform dashSprite4;
	public Transform dashSprite5;
	public Transform dashSprite6;

	public int i = 0;
	public float powerUpTime;
	public float powerUpD;
	public float dashForce = 700;

	Derp derp;


	void Start () {
		derp = Derp.instance;
	}
	

	void Update () {
		
		StartCoroutine (QueueSpawn ());
		Destroy (gameObject, powerUpD);

		/*if (Input.GetKeyDown (KeyCode.A)) 
		{

				rigidbody2D.AddForce(new Vector2(dashForce, 0));

		}*/

		if (Input.GetKeyDown (KeyCode.X)) 
		{
			float multiplier = 1;
			if (!derp.facingRight)
			{
				multiplier = -1;
				rigidbody2D.AddForce(new Vector2(dashForce * multiplier, 0));
				Instantiate (dashSprite4, dashSpawn.position, dashSpawn.rotation);
				Instantiate (dashSprite5, dashSpawn2.position, dashSpawn2.rotation);
				Instantiate (dashSprite6, dashSpawn3.position, dashSpawn3.rotation);
			}
			else if (derp.facingRight)
			{
				rigidbody2D.AddForce(new Vector2(dashForce, 0));
				Instantiate (dashSprite, dashSpawn.position, dashSpawn.rotation);
				Instantiate (dashSprite2, dashSpawn2.position, dashSpawn2.rotation);
				Instantiate (dashSprite3, dashSpawn3.position, dashSpawn3.rotation);
			}
			
		}


		
	}


	
	IEnumerator QueueSpawn() 
	{
		yield return new WaitForSeconds(powerUpTime);
		Debug.Log("reverting");
		
		if (i == 0) {
			Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
			i++;
		}
	}
}
