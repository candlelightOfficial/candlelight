﻿using UnityEngine;
using System.Collections;

public class Scarecrow : MonoBehaviour {

	bool inRange = false;
	public GameObject sCrow;
	public Transform currentPoint;
	public float moveSpeed;
	public Collider2D col;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (inRange == true)
		sCrow.transform.position = Vector3.MoveTowards (sCrow.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{

		
		if (col.tag == "Player") {

			inRange = true;
		}
	}
}
