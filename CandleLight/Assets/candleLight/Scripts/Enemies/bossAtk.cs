﻿using UnityEngine;
using System.Collections;

public class bossAtk : MonoBehaviour {
	
	public GameObject bosAtk;
	public Transform currentPoint;
	public float moveSpeed;
	public Collider2D col;
	public float atkLife;
	
	
	// Use this for initialization
	void Start () {

		GameObject.Destroy (gameObject, atkLife);
		
	}
	
	// Update is called once per frame
	void Update () {
		

			bosAtk.transform.position = Vector3.MoveTowards (bosAtk.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);
		
		
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{

		
		if (col.tag == "Player") {
			Destroy (col.gameObject);
			
		}
	}


}
