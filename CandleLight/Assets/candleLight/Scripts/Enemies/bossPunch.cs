﻿using UnityEngine;
using System.Collections;

public class bossPunch : MonoBehaviour {


	public GameObject rightHand;
	public Transform punchPoint;
	public Transform returnPoint;
	public float moveSpeed;
	public float returnSpeed;
	public bool punchTime = false;
	public bool returnTime = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (punchTime == true) {
			rightHand.transform.position = Vector3.MoveTowards (rightHand.transform.position, punchPoint.position, Time.deltaTime * moveSpeed);
		}

		if (returnTime == true) {

			rightHand.transform.position = Vector3.MoveTowards (rightHand.transform.position, returnPoint.position, Time.deltaTime * returnSpeed);

		}


			if (rightHand.transform.position == punchPoint.position) 
			{
			    returnTime = true;
				punchTime = false;
				Debug.Log("reached");
		
				
			}
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		
		
		if (col.tag == "Player") {

			returnTime = false;
			punchTime = true;
			
		}
	}
}
