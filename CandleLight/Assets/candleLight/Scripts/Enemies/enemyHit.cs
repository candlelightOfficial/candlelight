﻿using UnityEngine;
using System.Collections;

public class enemyHit : MonoBehaviour {

	public float timeTillDead;

	//public float trapDelay = 0.5;
	Animator anim;
	bool dead = false;

	void Start () {
		anim = GetComponent<Animator> ();
	}

	[System.Serializable] 
	public class EnemyStats {
		public float enemyHealth = 100f;
	}

	public EnemyStats enemyStats = new EnemyStats();

	public int fallBoundary = -20;

	void Update(){
		if (transform.position.y <= fallBoundary)
			DamageEnemy (999999);

	}



	void OnCollisionEnter2D (Collision2D  collision){
		
		if (collision.gameObject.tag == "bullet") {
			Debug.Log ("ouch");
			Debug.Log (enemyStats.enemyHealth);
			DamageEnemy (10);
		    
			anim.SetTrigger ("takingDmg");
		}
			//takingDamage = true;

	}

	public void DamageEnemy (int damage) {
		enemyStats.enemyHealth -= damage;
		if (enemyStats.enemyHealth <= 0) {
			anim.SetBool ("dead", dead);
			dead = true;
			Destroy (transform.parent.gameObject, timeTillDead);
			
		}
	}

}
