﻿using UnityEngine;
using System.Collections;

public class enemyMove : MonoBehaviour {

	public GameObject enemy;
	
	public float moveSpeed;
	
	public Transform currentPoint;
	
	public Transform[] points;
	
	public int pointSelection;

	public bool facingRight = true;
	
	// Use this for initialization
	void Start () {
		
		currentPoint = points[pointSelection];
		
	}
	
	// Update is called once per frame
	void Update () {
		
		enemy.transform.position = Vector3.MoveTowards (enemy.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);
		
		if (enemy.transform.position == currentPoint.position) 
		{
			Debug.Log ("pointreached");
			Flip();
			pointSelection++;
			if(pointSelection == points.Length)
			{
				pointSelection = 0;

			}
			
			currentPoint = points[pointSelection];
			
		}
				
	}

	
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = enemy.transform.localScale;
		theScale.x *= -1;
		enemy.transform.localScale = theScale;
		
	}
}
