﻿using UnityEngine;
using System.Collections;

public class lHand : MonoBehaviour {

	public float spawnTimer;
	public GameObject rangeAttack;
	public Transform attackSpawnPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		CreateAttack ();
	
	}

	void CreateAttack()
	{
		spawnTimer -= Time.deltaTime;
		if (spawnTimer <= 0) {
			Instantiate (rangeAttack, attackSpawnPoint.position, Quaternion.identity);
			spawnTimer = 2f;
		}
	}
}
