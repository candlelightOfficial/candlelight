﻿using UnityEngine;
using System.Collections;

public class rHand : MonoBehaviour {

	public GameObject rightHand;
	public Transform currentPoint;
	public float moveSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		rightHand.transform.position = Vector2.MoveTowards (rightHand.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);
	
	}
}
