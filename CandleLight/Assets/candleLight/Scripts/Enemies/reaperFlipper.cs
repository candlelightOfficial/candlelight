﻿using UnityEngine;
using System.Collections;

public class reaperFlipper : MonoBehaviour {

	bool facingRight = false;
	bool turn = false;
	public Transform reapert;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (turn == true)
			Flip ();
	
		turn = false;
	}

	void OnTriggerEnter2D(Collider2D col)
	{

		if (col.tag == "Player") {
			Debug.Log ("triggred");
			turn = true;
		}

	}

	
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = reapert.transform.localScale;
		theScale.x *= -1;
		reapert.transform.localScale = theScale;
		
	}
}
