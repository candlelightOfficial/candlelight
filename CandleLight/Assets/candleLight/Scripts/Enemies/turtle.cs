﻿using UnityEngine;
using System.Collections;

public class turtle : MonoBehaviour {

	bool onYourLeft = false;

	public GameObject turtleEnemy;
	public Transform currentPoint;

	public float moveSpeed;
	public Collider2D col;

	public bool facingRight = true;
	public Transform[] points;
	public int pointSelection;
	
	// Use this for initialization
	void Start () {


		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (onYourLeft == true){
			turtleEnemy.transform.position = Vector3.MoveTowards (turtleEnemy.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);

			if (turtleEnemy.transform.position == currentPoint.position) 
			{
				Flip();
				onYourLeft = false;
				pointSelection++;

				if(pointSelection == points.Length)
				{
					pointSelection = 0;
					
				}
				
				currentPoint = points[pointSelection];
			}
	}

		
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		
		
		if (col.tag == "Player") {
			
			onYourLeft = true;

		}
	}


	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = turtleEnemy.transform.localScale;
		theScale.x *= -1;
		turtleEnemy.transform.localScale = theScale;
		
	}
}
