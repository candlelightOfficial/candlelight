﻿using UnityEngine;
using System.Collections;

public class bulletFlame : MonoBehaviour {

	public float bulletSpeed = 5;

	public float halter;

	public Derp player;

	//bool flameOn = false;

	// Use this for initialization
	void Start () {
	
		player = FindObjectOfType<Derp> ();

		if (player.transform.localScale.x < 0)
			bulletSpeed = -bulletSpeed;


	}
	
	// Update is called once per frame
	void FixedUpdate () {

//		transform.position += Vector3.right* bulletSpeed * Time.deltaTime;
//      

		rigidbody2D.velocity = new Vector2 (bulletSpeed, rigidbody2D.velocity.y);
		byFlame ();
	}

	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject.tag == "enemy")
		Destroy (gameObject, halter);
	}

	void byFlame()
	{
		GameObject.Destroy (gameObject, 3.0f);
		//flameOn = false;
	}



}
