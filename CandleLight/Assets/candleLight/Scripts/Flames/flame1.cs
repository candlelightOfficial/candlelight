﻿using UnityEngine;
using System.Collections;

public class flame1 : MonoBehaviour {

	public int pointsToAdd;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "flame") {

			FlameManager.AddFlame (pointsToAdd);

			Destroy (col.gameObject);
		}

		if (col.gameObject.tag == "purplePower") {
			Destroy (col.gameObject);
			Debug.Log ("poweredUpp");
		}
	}
}
