﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeathCount : MonoBehaviour {

	public static int deaths;
	
	Text text;
	
	void Start()
	{
		text = GetComponent<Text> ();
		
		deaths = 0;
	}
	
	void Update()
	{
		if (deaths < 0)
			deaths = 0;
		
		text.text = "" + deaths;
	}
	
	public static void AddDeath (int deathPlusOne)
	{
		deaths += deathPlusOne;
	}
}
