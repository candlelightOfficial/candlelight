﻿using UnityEngine;
using System.Collections;

public class GameMastr : MonoBehaviour {
	
	public static GameMastr gm;

	public int flamePen;
	public int deathCount;
	public Transform playerPrefab;
	public Transform spawnPoint;
	public GameObject flame;
	//public GameObject newflames;
//	public GameObject Allflames;
//	public Transform flamePoint;
	
	public Transform  flaPoint;
	public Transform  flaPoint1;
	public Transform flaPoint2;
	public Transform  flaPoint3;
	public Transform flaPoint4;
	public Transform  flaPoint5;
	public Transform flaPoint6;
	public Transform  flaPoint7;
	public Transform flaPoint8;
	public Transform  flaPoint9;
	public Transform  flaPoint10;
	public Transform flaPoint11;
	public Transform  flaPoint12;
	public Transform flaPoint13;
	public Transform  flaPoint14;
	public Transform flaPoint15;
	public Transform  flaPoint16;
	public Transform flaPoint17;
	public Transform flaPoint18;
	public Transform flaPoint19;
	public Transform  flaPoint20;
	public Transform flaPoint21;
	public Transform flaPoint22;
	public Transform flaPoint23;
	public Transform  flaPoint24;
	public Transform flaPoint25;
	public Transform flaPoint26;
	public Transform flaPoint27;
	public Transform  flaPoint28;
	public Transform flaPoint29;
	public Transform flaPoint30;
	public Transform flaPoint31;
	public Transform flaPoint32;
	public Transform  flaPoint33;
	public Transform flaPoint34;
	public Transform flaPoint35;
	public Transform flaPoint36;
	public Transform flaPoint37;
	public Transform  flaPoint38;
	public Transform flaPoint39;
	public Transform flaPoint40;
	public Transform flaPoint41;
	public Transform  flaPoint42;
	public Transform flaPoint43;
	public Transform flaPoint44;
	public Transform flaPoint45;
	public Transform flaPoint46;
	public Transform  flaPoint47;
	public Transform flaPoint48;
	public Transform  flaPoint49;
	public Transform flaPoint50;
	public Transform flaPoint51;
	public Transform flaPoint52;
	public Transform flaPoint53;
	public Transform  flaPoint54;
	public Transform flaPoint55;

	void Start (){
		if (gm == null) {
			gm = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMastr> ();
		}
	}



	public void RespawnPlayer (){

		StartCoroutine (QueueSpawn ());
		FlameManager.AddFlame (-flamePen);
		DeathCount.AddDeath (deathCount);
	}

	IEnumerator QueueSpawn() 
	{
		yield return new WaitForSeconds(1);
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}

	
	void RespawnFlames(){
		
		//Transform = new Transform[3];
		
//		Instantiate (Allflames, flamePoint.position, flamePoint.rotation);
		//for (int i = 0; i < flaPoint.Length; i++) {
		
		Instantiate (flame, flaPoint.position, flaPoint.rotation);
		Instantiate (flame, flaPoint1.position, flaPoint1.rotation);
		Instantiate (flame, flaPoint2.position, flaPoint2.rotation);
		Instantiate (flame, flaPoint3.position, flaPoint3.rotation);
		Instantiate (flame, flaPoint4.position, flaPoint4.rotation);
		Instantiate (flame, flaPoint5.position, flaPoint5.rotation);
		Instantiate (flame, flaPoint6.position, flaPoint6.rotation);
		Instantiate (flame, flaPoint7.position, flaPoint7.rotation);
		Instantiate (flame, flaPoint8.position, flaPoint8.rotation);
		Instantiate (flame, flaPoint9.position, flaPoint9.rotation);
		Instantiate (flame, flaPoint10.position, flaPoint10.rotation);
		Instantiate (flame, flaPoint11.position, flaPoint11.rotation);
		Instantiate (flame, flaPoint12.position, flaPoint12.rotation);
		Instantiate (flame, flaPoint13.position, flaPoint13.rotation);
		Instantiate (flame, flaPoint14.position, flaPoint14.rotation);
		Instantiate (flame, flaPoint15.position, flaPoint15.rotation);
		Instantiate (flame, flaPoint16.position, flaPoint16.rotation);
		Instantiate (flame, flaPoint17.position, flaPoint17.rotation);
		Instantiate (flame, flaPoint18.position, flaPoint18.rotation);
		Instantiate (flame, flaPoint19.position, flaPoint19.rotation);
		Instantiate (flame, flaPoint20.position, flaPoint20.rotation);
		Instantiate (flame, flaPoint21.position, flaPoint21.rotation);
		Instantiate (flame, flaPoint22.position, flaPoint22.rotation);
		Instantiate (flame, flaPoint23.position, flaPoint23.rotation);
		Instantiate (flame, flaPoint24.position, flaPoint24.rotation);
		Instantiate (flame, flaPoint25.position, flaPoint25.rotation);
		Instantiate (flame, flaPoint26.position, flaPoint26.rotation);
		Instantiate (flame, flaPoint27.position, flaPoint27.rotation);
		Instantiate (flame, flaPoint28.position, flaPoint28.rotation);
		Instantiate (flame, flaPoint29.position, flaPoint29.rotation);
		Instantiate (flame, flaPoint30.position, flaPoint30.rotation);
		Instantiate (flame, flaPoint31.position, flaPoint31.rotation);
		Instantiate (flame, flaPoint32.position, flaPoint32.rotation);
		Instantiate (flame, flaPoint33.position, flaPoint33.rotation);
		Instantiate (flame, flaPoint34.position, flaPoint34.rotation);
		Instantiate (flame, flaPoint35.position, flaPoint35.rotation);
		Instantiate (flame, flaPoint36.position, flaPoint36.rotation);
		Instantiate (flame, flaPoint37.position, flaPoint37.rotation);
		Instantiate (flame, flaPoint38.position, flaPoint38.rotation);
		Instantiate (flame, flaPoint39.position, flaPoint39.rotation);
		Instantiate (flame, flaPoint40.position, flaPoint40.rotation);
		Instantiate (flame, flaPoint41.position, flaPoint41.rotation);
		Instantiate (flame, flaPoint42.position, flaPoint42.rotation);
		Instantiate (flame, flaPoint43.position, flaPoint43.rotation);
		Instantiate (flame, flaPoint44.position, flaPoint44.rotation);
		Instantiate (flame, flaPoint45.position, flaPoint45.rotation);
		Instantiate (flame, flaPoint46.position, flaPoint46.rotation);
		Instantiate (flame, flaPoint47.position, flaPoint47.rotation);
		Instantiate (flame, flaPoint48.position, flaPoint48.rotation);
		Instantiate (flame, flaPoint49.position, flaPoint49.rotation);
		Instantiate (flame, flaPoint50.position, flaPoint50.rotation);
		Instantiate (flame, flaPoint51.position, flaPoint51.rotation);
		Instantiate (flame, flaPoint52.position, flaPoint52.rotation);
		Instantiate (flame, flaPoint53.position, flaPoint53.rotation);
		Instantiate (flame, flaPoint54.position, flaPoint54.rotation);
		Instantiate (flame, flaPoint55.position, flaPoint55.rotation);
		
		//}
		
	}
	
//	public void ridFlames()
//	{
//		Destroy (Allflames);
//	}
	
	public static void KillPlayer (Player player){
	
		Destroy (player.gameObject);

		gm.RespawnPlayer ();
//		gm.ridFlames ();
		gm.RespawnFlames ();
		Debug.Log ("Previous flames still in game");
	}
	
	
}
