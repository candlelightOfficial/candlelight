﻿using UnityEngine;
using System.Collections;

public class GameMastr2 : MonoBehaviour {
	
	public static GameMastr2 gm;
	
	public int flamePen;
	public int deathCount;
	public Transform playerPrefab;
	//private GameObject player;
	public Transform spawnPoint;
	public GameObject flame;
	public Transform  flaPoint;


	void Start (){


		if (gm == null) {
			gm = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMastr2> ();
		}
	}
	
	
	
	public void RespawnPlayer (){
		//player =  (GameObject)Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
		StartCoroutine (QueueSpawn ());
		FlameManager.AddFlame (-flamePen);
		DeathCount.AddDeath (deathCount);
	}
	
	IEnumerator QueueSpawn() 
	{
		yield return new WaitForSeconds(1);
	  //  player =  (GameObject)
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}
	
	
	void RespawnFlames(){
		
		Instantiate (flame, flaPoint.position, flaPoint.rotation);
		
	}
	
//		public void ridFlames()
//		{
//		Debug.Log ("sandwhich");
//			Destroy (flame);
//		Debug.Log ("sandwhich");
//
//		}

	
	public static void KillPlayer (Player player){

		Destroy(player.gameObject);

		//Destroy (Player);
		//gm.ridFlames ();
		gm.RespawnFlames ();
		gm.RespawnPlayer ();


	}

		
}
