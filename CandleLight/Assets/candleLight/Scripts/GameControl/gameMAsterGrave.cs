﻿using UnityEngine;
using System.Collections;

public class gameMAsterGrave : MonoBehaviour {

	public static gameMAsterGrave gm;
	
	public int flamePen;
	public int deathCount;
	public Transform playerPrefab;
	public Transform spawnPoint;
	public GameObject flame;
	public Transform  flaPoint;
	
	void Start (){
		if (gm == null) {
			gm = GameObject.FindGameObjectWithTag ("GM2").GetComponent<gameMAsterGrave> ();
		}
	}
	
	
	
	public void RespawnPlayer (){
		
		StartCoroutine (QueueSpawn ());
		FlameManager.AddFlame (-flamePen);
		DeathCount.AddDeath (deathCount);
	}
	
	IEnumerator QueueSpawn() 
	{
		yield return new WaitForSeconds(1);
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}
	
	
	void RespawnFlames(){
		
		Instantiate (flame, flaPoint.position, flaPoint.rotation);
		
	}
	
	public void ridFlames()
	{
		Destroy (GameObject.FindWithTag ("graveflame"));
		
	}
	
	
	public static void KillPlayer (Player player){
		
		Destroy (player.gameObject);
		Debug.Log ("derp");

		gm.ridFlames ();
		Debug.Log ("derp");
		gm.RespawnFlames ();
		Debug.Log ("derp");
		gm.RespawnPlayer ();
		
		Debug.Log ("Previous flames still in game");
	}

}
