﻿using UnityEngine;
using System.Collections;

public class mainMenu : MonoBehaviour {

	public string StartLevel;
	
	public string levelSelect;
	
	public void NewGame()
	{
		Application.LoadLevel (StartLevel);
	}
	
	public void LevelSelect()
	{
		Application.LoadLevel (levelSelect);
	}
	
	public void QuitGame()
	{
		Application.Quit ();
	}

}
