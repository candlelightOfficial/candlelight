﻿using UnityEngine;
using System.Collections;

public class popUps : MonoBehaviour {
	

	public GameObject tutCanvas;

	public bool showTut;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (showTut) {
			tutCanvas.SetActive (true);

		} else {
			tutCanvas.SetActive (false);

		}
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
			showTut = true;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "Player") {
			showTut = false;
		}
	}


}
