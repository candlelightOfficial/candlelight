﻿using UnityEngine;
using System.Collections;

public class Trap1 : MonoBehaviour {

	public float trapTime;

void OnCollisionEnter2D(Collision2D col)
	{

		if (col.gameObject.name == "Trap_1") {
			Destroy(col.gameObject, trapTime);
		}
	}

}
