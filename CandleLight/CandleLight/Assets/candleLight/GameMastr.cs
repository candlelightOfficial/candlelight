﻿using UnityEngine;
using System.Collections;

public class GameMastr : MonoBehaviour {

	public static GameMastr gm;

	void Start (){
		if (gm == null) {
			gm = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMastr> ();
		}
	}

	public Transform playerPrefab;
	public Transform spawnPoint;
	public GameObject flame;
	public Transform flaPoint;
	public GameObject[] fla;

	public void RespawnPlayer (){
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}

	void RespawnFlames(){

		fla = new GameObject[3];
		for (int i = 0; i < fla.Length; i++)
		{
			GameObject clone = (GameObject)Instantiate(flame, flaPoint.position, flaPoint.rotation);
				fla[i] = clone;
		}
	}




    public static void KillPlayer (Player player){
		Destroy (player.gameObject);
		gm.RespawnPlayer ();
		gm.RespawnFlames ();
	}
}
